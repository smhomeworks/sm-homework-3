package restaurant_tests;

import static org.junit.Assert.*;

import org.junit.Test;

import restaurant.Restaurant;

public class RestaurantTest {
	
	Restaurant restaurant = new Restaurant();

	@Test
	public void testPaySuppliers() {
		
		restaurant.setSuppliersAmount(100);
		restaurant.paySuppliers();
		
		assertEquals(9900, restaurant.getBudget());
	}
	
	@Test
	public void testPayUtilities(){
		
		restaurant.payUtilities(500);
		
		assertEquals(9500, restaurant.getBudget());
	}
	
	@Test
	public void testPaySalaries(){
		
		restaurant.paySalaries();
		
		assertEquals(8800, restaurant.getBudget());	
	}
	
	@Test
	public void testPayTraining(){
		
		restaurant.payTraining(1000);
		
		assertEquals(9000, restaurant.getBudget());
	}
	
	@Test
	public void testPopulateTables(){	
		
		restaurant.populateTables();

	}
	
	@Test
	public void testProcessOrder(){
		
	}
	
	@Test
	public void testComputeClientStatistics(){
		
		restaurant.computeClientStatistics();
	}
	
	@Test
	public void testGetWaiters(){
			
		assertEquals("Waiters list should be size 3", 3, restaurant.getWaiters().size());
		assertTrue("Employees have Waiter", 
				restaurant.getEmployees().contains(restaurant.getWaiters().get(0)));
	}
	
	@Test
	public void testGetChef(){
			
		assertTrue("Employees have Chef",
				restaurant.getEmployees().contains(restaurant.getChef()));
	}
	
	@Test
	public void testGetBarman(){
			
		assertTrue("Employees have Barman",
				restaurant.getEmployees().contains(restaurant.getBarman()));
	}
	
	@Test
	public void testGetMainDishes(){
		
		assertEquals("Main dishes list should be size 5", 5, restaurant.getMainDishes().size());
		assertTrue("MenuItems contain Main Dish", 
				restaurant.getMenuItems().contains(restaurant.getMainDishes().get(0)));
	}
	
	@Test
	public void testGetBeverages(){
		
		assertEquals("Beverages list size should be 5", 5, restaurant.getBeverages().size());
		assertTrue("Menuitems contain Beverage",
				restaurant.getMenuItems().contains(restaurant.getBeverages().get(0)));
	}
	
	@Test
	public void testGetSuppliersAmount(){
		assertEquals(0, restaurant.getSuppliersAmount());
	}
	
	@Test
	public void testGetNumberOfOccupiedTables(){
		
		assertEquals("Should return 5 occupied tables when reputation points is >= 15", 5,
				restaurant.getNumberOfOcupiedTables());
		
		restaurant.setReputationPoints(30);
		
		assertEquals("Should return 5 occupied tables when reputation points is >= 30", 9,
				restaurant.getNumberOfOcupiedTables());
		
		restaurant.setReputationPoints(5);
		
		assertEquals("Should return 2 occupied tables when reputation points is < 15", 2,
				restaurant.getNumberOfOcupiedTables());
	}
}
