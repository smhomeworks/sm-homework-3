package restaurant_tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import restaurant.Beverage;
import restaurant.Client;
import restaurant.Employee;
import restaurant.Experience;
import restaurant.GameController;
import restaurant.MainDish;
import restaurant.MenuItem;
import restaurant.Player;
import restaurant.Quality;
import restaurant.Restaurant;
import restaurant.Table;
import restaurant.Waiter;

public class GameControllerTest {
	
	GameController game = new GameController();
	Restaurant restaurant = new Restaurant();
	Employee employee = new Employee();
	Player player = new Player();
	Waiter waiter = new Waiter("Waiter", "Jack");
	Table table = new Table(1);

	@Test
	public void testGameController() {
				
		assertNotNull(game.getPlayer());
		assertNotNull(game.getRankingList());
	}

	@Test
	public void testChooseName() {

		game.chooseName("");
		player.setName("Jack");

		assertEquals("Jack", player.getName());
	}

	@Test
	public void testStartGame() {

		game.startGame();

		assertNotNull(game.getRestaurant());
	}

	@Test
	public void testTrainEmployee() {

		game.setRestaurant(new Restaurant());
		game.trainEmployee(waiter);

		assertEquals("Verify that trainingCost is 800", 800,
				waiter.getTrainingCost());

		assertEquals("Verify waiter has experience increased to medium",
				Experience.medium, waiter.getExperience());
	}

	@Test
	public void testMakeSelection() {

		game.setRestaurant(new Restaurant());
		game.makeSelection(waiter, table);

		assertEquals("Verify that table1 has waiter2 assigned",
				table.getWaiter(), waiter);
	}
	
	@Test
	public void testSetDishesQuality(){
		
		game.setRestaurant(new Restaurant());
		java.util.List<MainDish> mainDishes = game.getRestaurant().getMainDishes();
		game.setDishesQuality(2, 3);
		
		assertEquals(Quality.high, mainDishes.get(0).getQualityLevel());
		assertEquals(Quality.high, mainDishes.get(1).getQualityLevel());
		assertEquals(Quality.low, mainDishes.get(2).getQualityLevel());
		assertEquals(Quality.low, mainDishes.get(3).getQualityLevel());
		assertEquals(Quality.low, mainDishes.get(4).getQualityLevel());
	}
	
	@Test
	public void testSetBeveragesQuality(){
		
		game.setRestaurant(new Restaurant());
		java.util.List<Beverage> beverages = game.getRestaurant().getBeverages();
		game.setBeveragesQuality(3, 2);
		
		assertEquals(Quality.high, beverages.get(0).getQualityLevel());
		assertEquals(Quality.high, beverages.get(1).getQualityLevel());
		assertEquals(Quality.high, beverages.get(2).getQualityLevel());
		assertEquals(Quality.low, beverages.get(3).getQualityLevel());
		assertEquals(Quality.low, beverages.get(4).getQualityLevel());
	}
	
	@Test
	public void testSetPrice(){
		
		game.setRestaurant(new Restaurant());
		java.util.List<MenuItem> menuItems = game.getRestaurant().getMenuItems();
		game.setDishesQuality(4, 1);
		game.setBeveragesQuality(3, 2);
		game.setPrice(10, 25, 5, 15);
		
		assertEquals(25, menuItems.get(0).getPrice());
		assertEquals(25, menuItems.get(1).getPrice());
		assertEquals(25, menuItems.get(2).getPrice());
		assertEquals(25, menuItems.get(3).getPrice());
		assertEquals(10, menuItems.get(4).getPrice());
		assertEquals(15, menuItems.get(5).getPrice());
		assertEquals(15, menuItems.get(6).getPrice());
		assertEquals(15, menuItems.get(7).getPrice());
		assertEquals(5, menuItems.get(8).getPrice());
		assertEquals(5, menuItems.get(9).getPrice());
	}
	
	@Test
	public void testProcessOrdersOfDay(){
		
		game.setRestaurant(new Restaurant());
		restaurant.setClients(new ArrayList<Client>());
		restaurant.populateTables();
		
		game.processOrdersOfDay(1);
	}

	@Test
	public void testSetPlayer() {

		game.setPlayer(player);

		player.setName("Player");
		player.setScore(10000);

		assertNotNull(game.getPlayer());
		assertEquals(10000, player.getScore());
		assertEquals("Player", player.getName());
	}

}