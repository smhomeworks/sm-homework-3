/**
 * @(#) Table.java
 */

package restaurant;

public class Table
{
	private int number;
	
	private Waiter waiter;
	
	private java.util.List<Client> clients;
	
	public Table( int number )
	{
		this.number = number;
	}
	
	public void setNumber( int number )
	{
		this.number=number;
	}
	
	public int getNumber( )
	{
		return number;
	}
	
	public void setWaiter( Waiter waiter )
	{
		this.waiter=waiter;
	}
	
	public Waiter getWaiter( )
	{
		return waiter;
	}
	
	public void setClients( java.util.List<Client> clients )
	{
		this.clients=clients;
	}
	
	public java.util.List<Client> getClients( )
	{
		return clients;
	}
	
}
