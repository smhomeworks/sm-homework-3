/**
 * @(#) MainDish.java
 */

package restaurant;

public class MainDish extends MenuItem
{
	private int calorieCount;
	
	
	public MainDish( String name, int calorieCount )
	{
		this.name = name;
		this.calorieCount = calorieCount;
		this.qualityLevel = Quality.low;
		this.price = 0;
	}
	
	public int computeProductionPrice( )
	{
		if(this.qualityLevel == Quality.low){
			return 3;
		}
		if(this.qualityLevel == Quality.high){
		    return 10;
		}
		return 0;
	}
	
	public void setCalorieCount( int calorieCount )
	{
		this.calorieCount=calorieCount;
	}
	
	public int getCalorieCount( )
	{
		return calorieCount;
	}
	
	
}
