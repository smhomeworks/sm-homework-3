/**
 * @(#) GameController.java
 */

package restaurant;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class GameController
{
	private Player player;
	
	private Restaurant restaurant;
	
	private RankingList rankingList;
	
	public GameController( )
	{
		player = new Player();
		rankingList = new RankingList();
	}
	
	public void chooseName( String name )
	{
		this.player.setName(name);
	}
	
	public void startGame( )
	{
		restaurant = new Restaurant();
	}
	
	public void trainEmployee( Employee employee )
	{
		this.restaurant.payTraining(employee.getTrainingCost());
		employee.increaseExperience();
	}
	
	public void makeSelection( Employee waiter, Table table )
	{
		table.setWaiter((Waiter)waiter);
	}
	
	public void setDishesQuality( int highNo, int lowNo )
	{
		java.util.List<MainDish> mainDishes = this.getRestaurant().getMainDishes();
		
		for(int i = 0; i< mainDishes.size(); i++)
		{
			if(i < highNo)
			{
				mainDishes.get(i).setQualityLevel(Quality.high);
			}
			else
			{
				mainDishes.get(i).setQualityLevel(Quality.low);
			}
		}
	}
	
	public void setBeveragesQuality( int highNo, int lowNo )
	{
		java.util.List<Beverage> beverages = this.getRestaurant().getBeverages();
		
		for(int i = 0; i< beverages.size(); i++)
		{
			if(i < highNo)
			{
				beverages.get(i).setQualityLevel(Quality.high);
			}
			else
			{
				beverages.get(i).setQualityLevel(Quality.low);
			}
		}
	}
	
	public void setPrice( int lowDCost, int highDCost, int lowBCost, int highBCost )
	{
		java.util.List<MenuItem> menuItems = this.restaurant.getMenuItems();
		
		for(int i = 0; i<menuItems.size(); i++)
		{
			MenuItem item = menuItems.get(i);
			
			if(item.getClass() == MainDish.class)
			{
				if(item.getQualityLevel() == Quality.low)
				{
					item.setPrice(lowDCost);
				}
				
				if(item.getQualityLevel() == Quality.high)
				{
					item.setPrice(highDCost);
				}
			}
			
			if(item.getClass() == Beverage.class)
			{
				if(item.getQualityLevel() == Quality.low)
				{
					item.setPrice(lowBCost);
				}
				
				if(item.getQualityLevel() == Quality.high)
				{
					item.setPrice(highBCost);
				}
			}
		}
	}
	
	public void setPlayer( Player player )
	{
		this.player=player;
	}
	
	public Player getPlayer( )
	{
		return player;
	}
	
	public void setRestaurant( Restaurant restaurant )
	{
		this.restaurant=restaurant;
	}
	
	public Restaurant getRestaurant( )
	{
		return restaurant;
	}
	
	public void processOrdersOfDay( int day)
	{
		int tablesOccupied = this.getRestaurant().getNumberOfOcupiedTables();
		java.util.List<Table> remainingTables = new ArrayList<Table>();
		remainingTables.addAll(this.getRestaurant().getTables());
		
		for(int t = 0; t < tablesOccupied; t++)
		{
			Random rnd = new Random();
			int tableNumber = rnd.nextInt(remainingTables.size());
			Table table = remainingTables.get(tableNumber);
			remainingTables.remove(tableNumber);
			
			Client client1 = table.getClients().get(0);
			Client client2 = table.getClients().get(1);
			
			this.restaurant.processOrder(table, client1, day);
			this.restaurant.processOrder(table, client2, day);
		}
	}
	
	public void assignWaiters( BufferedReader reader )
	{
		System.out.println("\nAssign Tables:\n");
		
		java.util.List<Waiter> waiters = this.getRestaurant().getWaiters();
		int fistWaiterTables = 0;
		int secondWaiterTables = 0;
		int thirdWaiterTables = 0;
		
		boolean selection = false;
		
		for(int t = 0; t < this.getRestaurant().getTables().size(); t++)
		{
			selection = false;
			while(!selection)
			{
				System.out.println("Please assign waiter to table " + (t+1) + ":");
				
				Table table = this.getRestaurant().getTables().get(t);
				table.setWaiter(null);
				
				int waiterNumber = 0;
				try {
					waiterNumber = Integer.parseInt(reader.readLine());
					selection = true;
					
					if(waiterNumber < 1 || waiterNumber > 3)
					{
						//waiterNumber = 1;
						selection = false;
					}
					else
					{
						switch(waiterNumber)
						{
							case 1:
								if(fistWaiterTables >= 3)
								{
									selection = false;
								}
								else
								{
									fistWaiterTables++;
								}
								break;
							case 2:
								if(secondWaiterTables >= 3)
								{
									selection = false;
								}
								else
								{
									secondWaiterTables++;
								}
								break;
							case 3:
								if(thirdWaiterTables >= 3)
								{
									selection = false;
								}
								else
								{
									thirdWaiterTables++;
								}
								break;
						}
					}
				} catch (NumberFormatException e) {
					//waiterNumber = 1;
					selection = false;
					e.printStackTrace();
				} catch (IOException e) {
					//waiterNumber = 1;
					selection = false;
					e.printStackTrace();
				}
				
				if(selection)
				{
					Waiter waiter = waiters.get(waiterNumber - 1);
					this.makeSelection(waiter, table);
					
					System.out.println("Waiter " + waiter.getSurname() + " is searving table " + table.getNumber());
				}
				else
				{
					System.out.println("Invalid selection or the number of tables have been exceeded. Try again");
				}
			}
			
		}
	}
	
	public void setRankingList( RankingList rankingList )
	{
		this.rankingList=rankingList;
	}
	
	public RankingList getRankingList( )
	{
		return rankingList;
	}
	
	
}
