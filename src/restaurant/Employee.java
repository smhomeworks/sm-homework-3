/**
 * @(#) Employee.java
 */

package restaurant;

public class Employee
{
	protected String name;
	
	protected String surname;
	
	protected int salary;
	
	protected Experience experience;
	
	private void computeSalary( )
	{
		
	}
	
	public void increaseExperience( )
	{
		
	}
	
	public void setExperience( Experience experience )
	{
		this.experience=experience;
	}
	
	public Experience getExperience( )
	{
		return experience;
	}
	
	public void setName( String name )
	{
		this.name=name;
	}
	
	public String getName( )
	{
		return name;
	}
	
	public void setSalary( int salary )
	{
		this.salary=salary;
	}
	
	public int getSalary( )
	{
		return salary;
	}
	
	public void setSurname( String surname )
	{
		this.surname=surname;
	}
	
	public String getSurname( )
	{
		return surname;
	}
	
	public int getTrainingCost( )
	{
		return 0;
	}
	
	
}
