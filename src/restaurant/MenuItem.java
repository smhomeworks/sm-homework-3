/**
 * @(#) MenuItem.java
 */

package restaurant;

public class MenuItem
{
	protected String name;
	
	protected Quality qualityLevel;
	
	protected int price;
	
	public int computeProductionPrice( )
	{
		return 0;
	}
	
	public void setName( String name )
	{
		this.name=name;
	}
	
	public String getName( )
	{
		return name;
	}
	
	public void setPrice( int price )
	{
		this.price=price;
	}
	
	public int getPrice( )
	{
		return price;
	}
	
	public void setQualityLevel( Quality qualityLevel )
	{
		this.qualityLevel=qualityLevel;
	}
	
	public Quality getQualityLevel( )
	{
		return qualityLevel;
	}
	
	
}
