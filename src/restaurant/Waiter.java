/**
 * @(#) Waiter.java
 */

package restaurant;

public class Waiter extends Employee
{
	public Waiter( String name, String surname )
	{
		this.name = name;
		this.surname = surname;
		this.experience = Experience.low;
		this.computeSalary();
	}
	
	private void computeSalary( )
	{
		switch(this.experience)
		{
			case low:
				this.salary = 200;
				break;
			case medium:
				this.salary = 300;
				break;
			case high:
				this.salary = 400;
				break;
			default:
				this.salary = 200;
				break;
		}
	}
	
	public void increaseExperience( )
	{
		switch(this.experience)
		{
			case low:
				this.experience = Experience.medium;
				break;
			case medium:
				this.experience = Experience.high;
				break;
			case high:
				break;
			default:
				break;
		}
	}
	
	public int getTrainingCost( )
	{
		return 800;
	}
	
	
}
