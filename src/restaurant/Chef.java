/**
 * @(#) Chef.java
 */

package restaurant;

public class Chef extends Employee
{
	private String taxCode;
	
	public Chef( String name, String surname )
	{
		this.name = name;
		this.surname = surname;
		this.experience = Experience.low;
		this.computeSalary();
		this.taxCode = "ABC123";
	}
	
	private void computeSalary( )
	{
		switch(this.experience)
		{
			case low:
				this.salary = 300;
				break;
			case medium:
				this.salary = 400;
				break;
			case high:
				this.salary = 500;
				break;
			default:
				this.salary = 300;
				break;
		}
	}
	
	public void increaseExperience( )
	{
		switch(this.experience)
		{
			case low:
				this.experience = Experience.medium;
				break;
			case medium:
				this.experience = Experience.high;
				break;
			case high:
				break;
			default:
				break;
		}
	}
	
	public void setTaxCode( String taxCode )
	{
		this.taxCode=taxCode;
	}
	
	public String getTaxCode( )
	{
		return taxCode;
	}
	
	public int getTrainingCost( )
	{
		return 1200;
	}
	
	
}
