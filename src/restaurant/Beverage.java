/**
 * @(#) Beverage.java
 */

package restaurant;

public class Beverage extends MenuItem
{
	private int volume;
	
	
	public Beverage( String name, int volume )
	{
		this.name = name;
		this.volume = volume;
		this.qualityLevel = Quality.low;
		this.price = 0;
	}
	
	public int computeProductionPrice( )
	{
		if(this.qualityLevel == Quality.low){
			return 1;
		}
		if(this.qualityLevel == Quality.high){
		    return 3;
		}
		return 0;
	}
	
	public void setVolume( int volume )
	{
		this.volume=volume;
	}
	
	public int getVolume( )
	{
		return volume;
	}
	
	
}
