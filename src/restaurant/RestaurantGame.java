/**
 * @(#) RestaurantGame.java
 */

package restaurant;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class RestaurantGame
{
	private static GameController controller;
	
	public static void main( String [] arguments )
	{
		
		BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
		
		controller = new GameController();
		
		System.out.println("Welcome to The Restaurant Game\n");
		
		System.out.println("Please enter your name:");
		
		try {
			controller.chooseName(bufferRead.readLine());
		} catch (IOException e) {
			controller.chooseName("No Name");
			e.printStackTrace();
		}
		
		controller.startGame();
		
		System.out.println("Welcome " + controller.getPlayer().getName());
		System.out.println("Restaurant Details:");
		System.out.println("	Name: " + controller.getRestaurant().getName());
		System.out.println("	Address: " + controller.getRestaurant().getAddress());
		System.out.println("	City: " + controller.getRestaurant().getCity());
		System.out.println("	Budget: " + controller.getRestaurant().getBudget());
		System.out.println("	Reputation Points: " + controller.getRestaurant().getReputationPoints());
		
		System.out.println("\nEmployees:");
		
		for(int i = 0;i < controller.getRestaurant().getWaiters().size();i++)
		{
			Waiter wt = controller.getRestaurant().getWaiters().get(i);
			System.out.println("	Name: " + wt.getName() + " " + wt.getSurname() + 
					", Experience Level: " + wt.getExperience() + ", Salary: " + wt.getSalary());
		}
		
		Chef cf = controller.getRestaurant().getChef();
		System.out.println("	Name: " + cf.getName() + " " + cf.getSurname() + 
				", Experience Level: " + cf.getExperience() + ", Salary: " + cf.getSalary() + 
				", Tax Code: " + cf.getTaxCode());
		
		Barman bm = controller.getRestaurant().getBarman();
		System.out.println("	Name: " + bm.getName() + " " + bm.getSurname() + 
				", Experience Level: " + bm.getExperience() + ", Salary: " + bm.getSalary());
		
		System.out.println("\nIn the restaurant there are " + controller.getRestaurant().getTables().size() + " tables");
		
		System.out.println("\nClients:");
		
		for(int i = 0; i < controller.getRestaurant().getClients().size(); i++)
		{
			Client client = controller.getRestaurant().getClients().get(i);
			
			System.out.println("	Name: " + client.getName() + " " + client.getSurname() 
					+ ", Telephone: " + client.getTelephone() + ", Tax Code: " + client.getTaxCode());
		}
		
		System.out.println("\n-------MENU------------");
		
		System.out.println("\nMain Dishes:");
		for(int i = 0;i < controller.getRestaurant().getMainDishes().size();i++)
		{
			MainDish md = controller.getRestaurant().getMainDishes().get(i);
			System.out.println("	Main Dish: " + md.getName()  + ", Calorie Count: " + md.getCalorieCount());
		}
		
		System.out.println("\nBeverages:");
		for(int i = 0;i < controller.getRestaurant().getBeverages().size();i++)
		{
			Beverage bv = controller.getRestaurant().getBeverages().get(i);
			System.out.println("	Beverages: " + bv.getName()  + ", Calorie Count: " + bv.getVolume());
		}
		
		/////////////Define Menu////////////////////////
		System.out.println("\nPlease enter the number of high quality main dishes (out of 5):");
		
		int highQualityDishesNo;
		try {
			highQualityDishesNo = Integer.parseInt(bufferRead.readLine());
		} catch (NumberFormatException e) {
			highQualityDishesNo = 0;
			e.printStackTrace();
		} catch (IOException e) {
			highQualityDishesNo = 0;
			e.printStackTrace();
		}
		
		if(highQualityDishesNo > 5)
		{
			highQualityDishesNo = 5;
		}
		if(highQualityDishesNo < 0)
		{
			highQualityDishesNo = 0;
		}
		
		int lowQualityDishesNo = controller.getRestaurant().getMainDishes().size() - highQualityDishesNo;
		
		controller.setDishesQuality(highQualityDishesNo, lowQualityDishesNo);
		
		System.out.println("You've defined " + highQualityDishesNo + " high quality main dishes and " 
							+ lowQualityDishesNo + " low quality main dishes");
		
		System.out.println("\nPlease enter the number of high quality bverages (out of 5):");
		
		int highQualityBeveragesNo;
		try {
			highQualityBeveragesNo = Integer.parseInt(bufferRead.readLine());
		} catch (NumberFormatException e) {
			highQualityBeveragesNo = 0;
			e.printStackTrace();
		} catch (IOException e) {
			highQualityBeveragesNo = 0;
			e.printStackTrace();
		}
		
		if(highQualityBeveragesNo > 5)
		{
			highQualityBeveragesNo = 5;
		}
		if(highQualityBeveragesNo < 0)
		{
			highQualityBeveragesNo = 0;
		}
		
		int lowQualityBeveragesNo = controller.getRestaurant().getMainDishes().size() - highQualityBeveragesNo;
		
		controller.setBeveragesQuality(highQualityBeveragesNo, lowQualityBeveragesNo);
		
		System.out.println("You've defined " + highQualityBeveragesNo + " high quality beverages and " 
				+ lowQualityBeveragesNo + " low quality beverages");
		
		System.out.println("\nPlease enter the price for low quality main dishes:");
		
		int lowQualityMainDishesPrice;
		
		try {
			lowQualityMainDishesPrice = Integer.parseInt(bufferRead.readLine());
		} catch (NumberFormatException e) {
			lowQualityMainDishesPrice = 3;
			e.printStackTrace();
		} catch (IOException e) {
			lowQualityMainDishesPrice = 3;
			e.printStackTrace();
		}
		if(lowQualityMainDishesPrice < 0)
		{
			lowQualityMainDishesPrice = 0;
		}
		
		System.out.println("\nPlease enter the price for high quality main dishes:");
		
		int highQualityMainDishesPrice;
		
		try {
			highQualityMainDishesPrice = Integer.parseInt(bufferRead.readLine());
		} catch (NumberFormatException e) {
			highQualityMainDishesPrice = 10;
			e.printStackTrace();
		} catch (IOException e) {
			highQualityMainDishesPrice = 10;
			e.printStackTrace();
		}
		if (highQualityMainDishesPrice < 0)
		{
			highQualityMainDishesPrice = 0;
		}
		
		System.out.println("\nPlease enter the price for low quality beverages:");
		
		int lowQualityBeveragesPrice;
		
		try {
			lowQualityBeveragesPrice = Integer.parseInt(bufferRead.readLine());
		} catch (NumberFormatException e) {
			lowQualityBeveragesPrice = 1;
			e.printStackTrace();
		} catch (IOException e) {
			lowQualityBeveragesPrice = 1;
			e.printStackTrace();
		}
		if(lowQualityBeveragesPrice < 0)
		{
			lowQualityBeveragesPrice = 0;
		}
		
		System.out.println("\nPlease enter the price for high quality beverages:");
		
		int highQualityBeveragesPrice;
		
		try {
			highQualityBeveragesPrice = Integer.parseInt(bufferRead.readLine());
		} catch (NumberFormatException e) {
			highQualityBeveragesPrice = 3;
			e.printStackTrace();
		} catch (IOException e) {
			highQualityBeveragesPrice = 3;
			e.printStackTrace();
		}
		if(highQualityBeveragesPrice < 0)
		{
			highQualityBeveragesPrice = 0;
		}
		controller.setPrice(lowQualityMainDishesPrice, highQualityMainDishesPrice, lowQualityBeveragesPrice, highQualityBeveragesPrice);
		
		System.out.println("\nPrice list:");
		System.out.println("Low Quality Main Dishes: " + lowQualityMainDishesPrice);
		System.out.println("High Quality Main Dishes: " + highQualityMainDishesPrice);
		System.out.println("Low Quality Beverages: " + lowQualityBeveragesPrice);
		System.out.println("High Quality Beverages: " + highQualityBeveragesPrice);
		
		
		//Daily Loop//////////////////////////////////////////////////////////////////////
		for(int i=1; i<=30; i++)
		{
			System.out.println("------------------DAY " + i + "--------------------------");
			
			//Train Employees///////////////////////////////////////////
			if(i % 7 == 1)
			{
				boolean trainEmployees = true;
				while(trainEmployees)
				{
					System.out.println("\nPlease select employees to train");
					System.out.println("\nPlease select employees to train");
					
					for(int e = 1; e <= controller.getRestaurant().getEmployees().size(); e++)
					{
						Employee em = controller.getRestaurant().getEmployees().get(e - 1);
						
						System.out.println("	[" + e + "] " + "Name: " + em.getName() + " " + em.getSurname() + 
										", Experience Level: " + em.getExperience() + ", Train Cost: " + em.getTrainingCost());
					}
					System.out.println("	[6] DONE");
					
					String option = "";
					try {
						 option = bufferRead.readLine();
					} catch (IOException e) {
						trainEmployees = false;
					}
					
					Employee em;
					switch (option) {
					case "1":
						em = controller.getRestaurant().getEmployees().get(0);
						if(em.getTrainingCost() <= controller.getRestaurant().getBudget() && em.getExperience() != Experience.high)
						{
							controller.trainEmployee(em);
						}
						else
						{
							System.out.println("Insuficient money in the budget for this action or eomployee already has a high level experience");
						}
						break;
					case "2":
						em = controller.getRestaurant().getEmployees().get(1);
						if(em.getTrainingCost() <= controller.getRestaurant().getBudget() && em.getExperience() != Experience.high)
						{
							controller.trainEmployee(em);
						}
						else
						{
							System.out.println("Insuficient money in the budget for this action or eomployee already has a high level experience");
						}
						break;
					case "3":
						em = controller.getRestaurant().getEmployees().get(2);
						if(em.getTrainingCost() <= controller.getRestaurant().getBudget() && em.getExperience() != Experience.high)
						{
							controller.trainEmployee(em);
						}
						else
						{
							System.out.println("Insuficient money in the budget for this action or eomployee already has a high level experience");
						}
						break;
					case "4":
						em = controller.getRestaurant().getEmployees().get(3);
						if(em.getTrainingCost() <= controller.getRestaurant().getBudget() && em.getExperience() != Experience.high)
						{
							controller.trainEmployee(em);
						}
						else
						{
							System.out.println("Insuficient money in the budget for this action or eomployee already has a high level experience");
						}
						break;
					case "5":
						em = controller.getRestaurant().getEmployees().get(4);
						if(em.getTrainingCost() <= controller.getRestaurant().getBudget() && em.getExperience() != Experience.high)
						{
							controller.trainEmployee(em);
						}
						else
						{
							System.out.println("Insuficient money in the budget for this action or eomployee already has a high level experience");
						}
						break;
					default:
						trainEmployees = false;
						break;
					}
				}
			}
			////////////////////////////////////////////////////////////
			
			controller.assignWaiters(bufferRead);
			
			System.out.println("\nPopulating tables with clients... ");
			controller.getRestaurant().populateTables();
			
			
			System.out.println("\nProcessing orders: ");
			controller.processOrdersOfDay(i);
			
			System.out.println("\nBudget at the end of day " + i + " is: " + controller.getRestaurant().getBudget());
			System.out.println("\nReputation points at the end of the day " + i + ": " + controller.getRestaurant().getReputationPoints());
			
			
			//Actions done at the end of the week
			if(i % 7 == 0)
			{
				System.out.println("\nEnd of the week payments: ");
				controller.getRestaurant().paySuppliers();
				controller.getRestaurant().paySalaries();
				System.out.println("\nBudget at the end of week " + i + " is: " + controller.getRestaurant().getBudget());
			}
			
			if(controller.getRestaurant().getBudget() <= 0)
			{
				System.out.println("\n-----------GAME OVER: Budget less than 0---------");
				controller.getPlayer().setScore(0);
				break;
			}
		}
		
		controller.getRestaurant().payUtilities(4000);
		
		if(controller.getRestaurant().getBudget() <= 0)
		{
			System.out.println("\n-----------GAME OVER: Budget less than 0---------");
			controller.getPlayer().setScore(0);
		}
		else
		{
			System.out.println("\n-----------GAME COMPLETED: Your score is " + controller.getRestaurant().getBudget() + "---------");
			controller.getRestaurant().computeClientStatistics();
			controller.getPlayer().setScore(controller.getRestaurant().getBudget());
			controller.getRankingList().readHighScores();
			controller.getRankingList().addPlayer(controller.getPlayer());
			controller.getRankingList().displayRank();
		}
		
	}
	
}
