/**
 * @(#) Restaurant.java
 */

package restaurant;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Restaurant
{
	private String name;
	
	private String address;
	
	private String city;
	
	private int budget;
	
	private int reputationPoints;
	
	private int suppliersAmount;
	
	private java.util.List<Employee> employees;
	
	private java.util.List<MenuItem> menuItems;
	
	private java.util.List<Table> tables;
	
	private java.util.List<Order> orders;
	
	private java.util.List<Client> clients;
	
	public Restaurant( )
	{
		name = "Super Restaurant";
		address = "Raekoja Plats, no. 7";
		city = "Tartu";
		budget = 10000;
		suppliersAmount = 0;
		this.reputationPoints = 15;
		
		//Employees Initialization///////////////////////////////////////////
		employees = new ArrayList<Employee>();
		
		Waiter waiter1 = new Waiter("Waiter", "John");
		employees.add(waiter1);
		
		Waiter waiter2 = new Waiter("Waiter", "James");
		employees.add(waiter2);
		
		Waiter waiter3 = new Waiter("Waiter", "Ben");
		employees.add(waiter3);
		
		Chef chef = new Chef("Chef", "Bill");
		employees.add(chef);
		
		Barman barman = new Barman("Barman", "Michael");
		employees.add(barman);
		//////////////////////////////////////////////////////////////////////
		
		//Tables Initialization///////////////////////////////////////////////
		tables = new ArrayList<Table>();
		
		for(int i = 1; i <= 9; i++)
		{
			Table table = new Table(i);
			tables.add(table);
		}
		//////////////////////////////////////////////////////////////////////
		
		//Menu Items Initialization///////////////////////////////////////////
		menuItems = new ArrayList<MenuItem>();
		
		MainDish mainDish1 = new MainDish("Main Dish 1", 150);
		menuItems.add(mainDish1);
		
		MainDish mainDish2 = new MainDish("Main Dish 2", 200);
		menuItems.add(mainDish2);
		
		MainDish mainDish3 = new MainDish("Main Dish 3", 220);
		menuItems.add(mainDish3);
		
		MainDish mainDish4 = new MainDish("Main Dish 4", 180);
		menuItems.add(mainDish4);
		
		MainDish mainDish5 = new MainDish("Main Dish 5", 260);
		menuItems.add(mainDish5);
		
		Beverage beverage1 = new Beverage("Beverage 1", 150);
		menuItems.add(beverage1);
		
		Beverage beverage2 = new Beverage("Beverage 2", 400);
		menuItems.add(beverage2);
		
		Beverage beverage3 = new Beverage("Beverage 3", 500);
		menuItems.add(beverage3);
		
		Beverage beverage4 = new Beverage("Beverage 4", 450);
		menuItems.add(beverage4);
		
		Beverage beverage5 = new Beverage("Beverage 5", 450);
		menuItems.add(beverage5);
		//////////////////////////////////////////////////////////////////////
		
		//Clients Initialization//////////////////////////////////////////////
		this.clients = new ArrayList<Client>();
		
		Client client1 = new Client("Client", "1", "123", "123");
		clients.add(client1);
		
		Client client2 = new Client("Client", "2", "234", "234");
		clients.add(client2);
		
		Client client3 = new Client("Client", "3", "345", "345");
		clients.add(client3);
		
		Client client4 = new Client("Client", "4", "456", "456");
		clients.add(client4);
		
		Client client5 = new Client("Client", "5", "567", "567");
		clients.add(client5);
		
		Client client6 = new Client("Client", "6", "678", "678");
		clients.add(client6);
		
		Client client7 = new Client("Client", "7", "789", "789");
		clients.add(client7);
		
		Client client8 = new Client("Client", "8", "890", "890");
		clients.add(client8);
		
		Client client9 = new Client("Client", "9", "901", "901");
		clients.add(client9);
		
		Client client10 = new Client("Client", "10", "012", "012");
		clients.add(client10);
		
		Client client11 = new Client("Client", "11", "111", "111");
		clients.add(client11);
		
		Client client12 = new Client("Client", "12", "112", "112");
		clients.add(client12);
		
		Client client13 = new Client("Client", "13", "113", "113");
		clients.add(client13);
		
		Client client14 = new Client("Client", "14", "114", "114");
		clients.add(client14);
		
		Client client15 = new Client("Client", "15", "115", "115");
		clients.add(client15);
		
		Client client16 = new Client("Client", "16", "116", "116");
		clients.add(client16);
		
		Client client17 = new Client("Client", "17", "117", "117");
		clients.add(client17);
		
		Client client18 = new Client("Client", "18", "118", "118");
		clients.add(client18);
		//////////////////////////////////////////////////////////////////////
		
		this.orders = new ArrayList<Order>();
	}
	
	public void paySuppliers( )
	{
		System.out.println("	Paying suppliers: " + this.suppliersAmount);
		this.budget -= this.suppliersAmount;
		this.suppliersAmount = 0;
	}
	
	public ReputationLevel computeReputation( int clientSatisfaction )
	{
		return null;
	}
	
	public void payUtilities( int amount )
	{
		this.budget -= amount;
		System.out.println("\nPaying utilities: " + amount);
	}
	
	public void paySalaries( )
	{
		int salaries = 0;
		
		for(int i=0; i<this.employees.size(); i++)
		{
			Employee em = this.employees.get(i);
			salaries += em.getSalary();
		}
		this.budget -= salaries;
		
		System.out.println("	Paying salaries: " + salaries);
	}
	
	public void populateTables( )
	{
		java.util.List<Client> assignClients = new ArrayList<Client>();
		assignClients.addAll(this.clients);
		
		Random rnd = new Random();
		
		for(int i = 0; i < this.tables.size(); i++)
		{
			Table table = this.tables.get(i);
			
			int client1 = rnd.nextInt(assignClients.size());
			Client clientNo1 = assignClients.get(client1);
			assignClients.remove(client1);
			
			int client2 = rnd.nextInt(assignClients.size());
			Client clientNo2 = assignClients.get(client2);
			assignClients.remove(client2);
			
			java.util.List<Client> clientsList = new ArrayList<Client>();
			clientsList.add(clientNo1);
			clientsList.add(clientNo2);
			
			table.setClients(clientsList);
		}
	}
	
	public void computeClientStatistics( )
	{
		System.out.println("\n-----------------Clients Statistics---------------");
		
		List<List<Order>> ordersByCustomer = new ArrayList<List<Order>>();
		
		for(int c = 0; c < this.getClients().size(); c++)
		{
			ordersByCustomer.add(new ArrayList<Order>());
		}
		
		for(int o = 0; o < this.getOrders().size(); o++)
		{
			Order order = this.getOrders().get(o);
			int index = this.getClients().lastIndexOf(order.getClient());
			
			ordersByCustomer.get(index).add(order);
		}
		
		for(int c = 0; c < this.getClients().size(); c++)
		{
			Client client = this.getClients().get(c);
			
			System.out.println("\n-- " + client.getName() + " " + client.getSurname());
			
			List<Integer> dishes = new ArrayList<Integer>();
			dishes.add(0);
			dishes.add(0);
			dishes.add(0);
			dishes.add(0);
			dishes.add(0);
			
			List<Integer> beverages = new ArrayList<Integer>();
			beverages.add(0);
			beverages.add(0);
			beverages.add(0);
			beverages.add(0);
			beverages.add(0);
			
			float averageCalorie = 0;
			float averageVolume = 0;
			
			int moneySpent = 0;
			
			for(int o = 0; o<ordersByCustomer.get(c).size(); o++)
			{
				Order ord = ordersByCustomer.get(c).get(o);
				Beverage bev;
				MainDish dish;
				
				if(ord.getMenuItems().get(0).getClass() == MainDish.class)
				{
					bev = (Beverage)ord.getMenuItems().get(1);
					dish = (MainDish)ord.getMenuItems().get(0);
				}
				else
				{
					bev = (Beverage)ord.getMenuItems().get(0);
					dish = (MainDish)ord.getMenuItems().get(1);
				}
				
				int bevIndex = this.getBeverages().indexOf(bev);
				int dishIndex = this.getMainDishes().indexOf(dish);
				
				beverages.set(bevIndex, beverages.get(bevIndex) + 1);
				dishes.set(dishIndex, dishes.get(dishIndex) + 1);
				
				averageCalorie += dish.getCalorieCount();
				averageVolume += bev.getVolume();
				
				moneySpent += dish.getPrice();
				moneySpent += bev.getPrice();
			}
			
			averageCalorie = averageCalorie / ordersByCustomer.get(c).size();
			averageVolume = averageVolume / ordersByCustomer.get(c).size();
			
			for(int i = 0; i < this.getBeverages().size(); i++)
			{
				Beverage bev = this.getBeverages().get(i);
				System.out.println("	ordered " + bev.getName() + " " + beverages.get(i) + " times");
			}
			
			for(int i = 0; i < this.getMainDishes().size(); i++)
			{
				MainDish dish = this.getMainDishes().get(i);
				System.out.println("	ordered " + dish.getName() + " " + dishes.get(i) + " times");
			}
			
			System.out.println("	Average Calories: " + averageCalorie);
			System.out.println("	Average Volumes: " + averageVolume);
			System.out.println("	Money Spent: " + moneySpent);
		}
	}
	
	public void payTraining( int amount )
	{
		this.budget -= amount;
	}
	
	public void processOrder( Table table, Client client, int day )
	{
		Random rnd = new Random();
		
		Beverage beverage = this.getBeverages().get(rnd.nextInt(this.getBeverages().size()));
		MainDish dish = this.getMainDishes().get(rnd.nextInt(this.getMainDishes().size()));
		
		Order order = new Order(table, client, beverage, dish, this.orders.size() + 1, day);
		order.computeClientSatisfaction(this.getBarman(), this.getChef());
		this.orders.add(order);
		
		this.budget += beverage.price;
		this.address += dish.price;
		
		this.suppliersAmount += beverage.computeProductionPrice();
		this.suppliersAmount += dish.computeProductionPrice();
		
		this.reputationPoints += order.getClientSatisfaction();
		
		System.out.println("Client " + client.getName() + " " + client.getSurname() + " orders " 
				+ beverage.getName() + " and " + dish.getName() + " --- satisfaction points: " 
				+ order.getClientSatisfaction());
	}
	
	public void setAddress( String address )
	{
		this.address=address;
	}
	
	public String getAddress( )
	{
		return address;
	}
	
	public void setBudget( int budget )
	{
		this.budget=budget;
	}
	
	public int getBudget( )
	{
		return budget;
	}
	
	public void setCity( String city )
	{
		this.city=city;
	}
	
	public String getCity( )
	{
		return city;
	}
	
	public void setClients( java.util.List<Client> clients )
	{
		this.clients=clients;
	}
	
	public java.util.List<Client> getClients( )
	{
		return clients;
	}
	
	public void setEmployees( java.util.List<Employee> employees )
	{
		this.employees=employees;
	}
	
	public java.util.List<Employee> getEmployees( )
	{
		return employees;
	}
	
	public void setMenuItems( java.util.List<MenuItem> menuItems )
	{
		this.menuItems=menuItems;
	}
	
	public java.util.List<MenuItem> getMenuItems( )
	{
		return menuItems;
	}
	
	public void setName( String name )
	{
		this.name=name;
	}
	
	public String getName( )
	{
		return name;
	}
	
	public void setOrders( java.util.List<Order> orders )
	{
		this.orders=orders;
	}
	
	public java.util.List<Order> getOrders( )
	{
		return orders;
	}
	
	public void setReputationPoints( int reputationPoints )
	{
		this.reputationPoints=reputationPoints;
	}
	
	public int getReputationPoints( )
	{
		return reputationPoints;
	}
	
	public void setTables( java.util.List<Table> tables )
	{
		this.tables=tables;
	}
	
	public java.util.List<Table> getTables( )
	{
		return tables;
	}
	
	public java.util.List<Waiter> getWaiters( )
	{
		java.util.List<Waiter> waiters = new ArrayList<Waiter>();
		
		for(int i = 0; i < this.employees.size(); i++)
		{
			if(this.employees.get(i).getClass() == Waiter.class)
			{
				waiters.add((Waiter)this.employees.get(i));
			}
		}
		
		return waiters;
	}
	
	public Chef getChef( )
	{
		for(int i = 0; i < this.employees.size(); i++)
		{
			if(this.employees.get(i).getClass() == Chef.class)
			{
				return (Chef) this.employees.get(i);
			}
		}
		
		return null;
	}
	
	public Barman getBarman( )
	{
		for(int i = 0; i < this.employees.size(); i++)
		{
			if(this.employees.get(i).getClass() == Barman.class)
			{
				return (Barman) this.employees.get(i);
			}
		}
		
		return null;
	}
	
	public java.util.List<MainDish> getMainDishes( )
	{
		java.util.List<MainDish> mainDishes = new ArrayList<MainDish>();
		
		for(int i = 0; i < this.menuItems.size(); i++)
		{
			if(this.menuItems.get(i).getClass() == MainDish.class)
			{
				mainDishes.add((MainDish)this.menuItems.get(i));
			}
		}
		
		return mainDishes;
	}
	
	public java.util.List<Beverage> getBeverages( )
	{
		java.util.List<Beverage> beverages = new ArrayList<Beverage>();
		
		for(int i = 0; i < this.menuItems.size(); i++)
		{
			if(this.menuItems.get(i).getClass() == Beverage.class)
			{
				beverages.add((Beverage)this.menuItems.get(i));
			}
		}
		
		return beverages;
	}
	
	public void setSuppliersAmount( int suppliersAmount )
	{
		this.suppliersAmount=suppliersAmount;
	}
	
	public int getSuppliersAmount( )
	{
		return suppliersAmount;
	}
	
	public int getNumberOfOcupiedTables()
	{
		if(this.reputationPoints >= 30)
		{
			return 9;
		}
		else if(this.reputationPoints >= 15)
		{
			return 5;
		}
		else
		{
			return 2;
		}
	}
	
	
}
