/**
 * @(#) Client.java
 */

package restaurant;

public class Client
{
	private String name;
	
	private String surname;
	
	private String telephone;
	
	private String taxCode;
	
	public Client( String name, String surname, String telephone, String taxCode )
	{
		this.name = name;
		this.surname = surname;
		this.telephone = telephone;
		this.taxCode = taxCode;
	}
	
	public Boolean isSatisfied( )
	{
		return null;
	}
	
	public void computeStatistics( )
	{
		
	}
	
	public void setName( String name )
	{
		this.name=name;
	}
	
	public String getName( )
	{
		return name;
	}
	
	public void setSurname( String surname )
	{
		this.surname=surname;
	}
	
	public String getSurname( )
	{
		return surname;
	}
	
	public void setTaxCode( String taxCode )
	{
		this.taxCode=taxCode;
	}
	
	public String getTaxCode( )
	{
		return taxCode;
	}
	
	public void setTelephone( String telephone )
	{
		this.telephone=telephone;
	}
	
	public String getTelephone( )
	{
		return telephone;
	}
	
}
