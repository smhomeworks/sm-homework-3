/**
 * @(#) RankingList.java
 */

package restaurant;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class RankingList
{
	private java.util.List<Player> players;
	
	
	public RankingList()
	{
		
	}
	

	public void addPlayer( Player player )
	{
		if(players.size() == 0)
		{
			players.add(player);
		}
		else
		{
			for(int i = 0; i< players.size(); i++)
			{
				Player oldPlayer = players.get(i);
				
				if(player.getScore() > oldPlayer.getScore())
				{
					players.add(i, player);
					break;
				}
			}
		}
		
		if(players.indexOf(player) == -1)
		{
			players.add(player);
		}
	}
	
	
	public void displayRank( )
	{
		System.out.println("\nRANKING LIST:\n");
		
		File highscores = new File("./highscores");
		
		FileWriter fileWriter;
		
		try {
			fileWriter = new FileWriter(highscores,false);
            
			for (int i=0; i<players.size();i++)
            {
                Player rankingPlayer = this.players.get(i);
                
                String playerLine = rankingPlayer.getName() + " " + rankingPlayer.getScore();
                
                fileWriter.append(playerLine);
                if(i < players.size() - 1)
                {
                	fileWriter.append(System.getProperty("line.separator"));
                }
                
                System.out.println((i+1) + ". " + rankingPlayer.getName() + " - " + rankingPlayer.getScore());
            }
			
			fileWriter.close();
        } catch (IOException e) {
            System.out.println("\nCannot display players ranking");
        }  
	}
	
	
	public void setPlayers( java.util.List<Player> players )
	{
		this.players=players;
	}
	
	
	public java.util.List<Player> getPlayers( )
	{
		return players;
	}
	
	public void readHighScores()
	{
		players = new ArrayList<Player>();
		
		try(BufferedReader br = new BufferedReader(new FileReader(new File("./highscores")))) {
		    for(String line; (line = br.readLine()) != null; ) {
		        
		    	System.out.println(line);
		    	
		    	String[] fields = line.split(" ");
		    	
		    	Player player = new Player();
		    	player.setName(fields[0]);
		    	player.setScore(Integer.parseInt(fields[1]));
		    	
		    	players.add(player);
		    	
		    }
		} catch (FileNotFoundException e) {
			//System.out.println("\nCannot display players ranking");
		} catch (IOException e) {
			//System.out.println("\nCannot display players ranking");
		}
	}
	
	
}
