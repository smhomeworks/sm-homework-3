/**
 * @(#) Barman.java
 */

package restaurant;

public class Barman extends Employee
{
	public Barman( String name, String surname )
	{
		this.name = name;
		this.surname = surname;
		this.experience = Experience.low;
		this.computeSalary();
	}
	
	private void computeSalary( )
	{
		switch(this.experience)
		{
			case low:
				this.salary = 300;
				break;
			case medium:
				this.salary = 400;
				break;
			case high:
				this.salary = 500;
				break;
			default:
				this.salary = 300;
				break;
		}
	}
	
	public void increaseExperience( )
	{
		switch(this.experience)
		{
			case low:
				this.experience = Experience.medium;
				break;
			case medium:
				this.experience = Experience.high;
				break;
			case high:
				break;
			default:
				break;
		}
	}
	
	public int getTrainingCost( )
	{
		return 1200;
	}
	
	
}
