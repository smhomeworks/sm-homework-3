/**
 * @(#) Order.java
 */

package restaurant;

import java.util.ArrayList;
import java.util.Random;

public class Order
{
	private int time;
	
	private String orderNo;
	
	private int clientSatisfaction;
	
	private java.util.List<MenuItem> menuItems;
	
	private Table table;
	
	private Client client;
	
	public Order( Table table, Client client, MenuItem beverage, MenuItem mainDish, int orderNo, int day )
	{
		this.client = client;
		this.table = table;
		this.menuItems = new ArrayList<MenuItem>();
		this.menuItems.add(beverage);
		this.menuItems.add(mainDish);
		this.orderNo = Integer.toString(orderNo);
		this.time = day;
	}
	
	public void setClient( Client client )
	{
		this.client=client;
	}
	
	public Client getClient( )
	{
		return client;
	}
	
	public void setClientSatisfaction( int clientSatisfaction )
	{
		this.clientSatisfaction=clientSatisfaction;
	}
	
	public int getClientSatisfaction( )
	{
		return clientSatisfaction;
	}
	
	public void setMenuItems( java.util.List<MenuItem> menuItems )
	{
		this.menuItems=menuItems;
	}
	
	public java.util.List<MenuItem> getMenuItems( )
	{
		return menuItems;
	}
	
	public void setOrderNo( String orderNo )
	{
		this.orderNo=orderNo;
	}
	
	public String getOrderNo( )
	{
		return orderNo;
	}
	
	public void setTable( Table table )
	{
		this.table=table;
	}
	
	public Table getTable( )
	{
		return table;
	}
	
	public void setTime( int time )
	{
		this.time=time;
	}
	
	public int getTime( )
	{
		return time;
	}
	
	public void computeClientSatisfaction( Barman barman, Chef chef )
	{
		this.clientSatisfaction = 0;
		Random rnd = new Random();
		
		int firstPriceDifference = (this.menuItems.get(0).price - this.menuItems.get(0).computeProductionPrice())/3*10;
		int secondPriceDifference = (this.menuItems.get(1).price - this.menuItems.get(1).computeProductionPrice())/3*10;
		
		//Compute service satisfaction//////
		int serviceSatisfactionPercent = 0;
		switch(this.table.getWaiter().getExperience())
		{
			case low:
				serviceSatisfactionPercent = 60;
				break;
			case medium:
				serviceSatisfactionPercent = 80;
				break;
			case high:
				serviceSatisfactionPercent = 90;
				break;
		}
		serviceSatisfactionPercent -= firstPriceDifference;
		serviceSatisfactionPercent -= secondPriceDifference;
		
		if(rnd.nextInt(101) <= serviceSatisfactionPercent)
		{
			this.clientSatisfaction += 1;
		}
		else
		{
			this.clientSatisfaction -= 1;
		}
		////////////////////////////////////
		
		MainDish dish = null;
		Beverage beverage = null;
		
		if(this.getMenuItems().get(0).getClass() == MainDish.class)
		{
			dish = (MainDish)this.getMenuItems().get(0);
			beverage = (Beverage)this.getMenuItems().get(1);
		}
		else if(this.getMenuItems().get(0).getClass() == Beverage.class)
		{
			beverage = (Beverage)this.getMenuItems().get(0);
			dish = (MainDish)this.getMenuItems().get(1);
		}
		
		//Compute food satisfaction//////
		int foodSatisfactionPercent = 0;
		switch(chef.getExperience())
		{
			case low:
				foodSatisfactionPercent = 40;
				break;
			case medium:
				foodSatisfactionPercent = 60;
				break;
			case high:
				foodSatisfactionPercent = 80;
				break;
		}
		foodSatisfactionPercent -= firstPriceDifference;
		foodSatisfactionPercent -= secondPriceDifference;
		
		if(dish.getQualityLevel() == Quality.high)
		{
			foodSatisfactionPercent += 20;
		}
		
		if(rnd.nextInt(101) <= foodSatisfactionPercent)
		{
			this.clientSatisfaction += 1;
		}
		else
		{
			this.clientSatisfaction -= 1;
		}
		////////////////////////////////////
		
		//Compute drink satisfaction//////
		int drinkSatisfactionPercent = 0;
		switch(barman.getExperience())
		{
			case low:
				drinkSatisfactionPercent = 40;
				break;
			case medium:
				drinkSatisfactionPercent = 60;
				break;
			case high:
				drinkSatisfactionPercent = 80;
				break;
		}
		drinkSatisfactionPercent -= firstPriceDifference;
		drinkSatisfactionPercent -= secondPriceDifference;
		
		if(beverage.getQualityLevel() == Quality.high)
		{
			drinkSatisfactionPercent += 20;
		}
		
		if(rnd.nextInt(101) <= drinkSatisfactionPercent)
		{
			this.clientSatisfaction += 1;
		}
		else
		{
			this.clientSatisfaction -= 1;
		}
		////////////////////////////////////
		
	}
	
	
}
